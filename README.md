# web-regexp
A project to teach/lean JS with others while creating a convenience tool for every day tasks. 

Demo site: https://web-regexp.netlify.app/#/ 

![Screenshot](/screenshot.png?raw=true "Screenshot")

## Project setup command
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
