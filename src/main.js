import Vue from 'vue';
import './plugins/vuetify'
import App from './App.vue';
import router from './router';
import store from './store';
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(fas)

Vue.config.productionTip = false;

Vue.component('font-awesome-icon', FontAwesomeIcon)

new Vue({
  router,
  store,
  el: '#app',
  render: h => h(App),
}).$mount('#app');
