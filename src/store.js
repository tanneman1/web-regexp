import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);
function toFlag(label, value) {
  return {
    label: label,
    value: value
  };
}
export default new Vuex.Store({
  state: {
    flagNames: [toFlag("global", "g"),
    toFlag("case insensitive", "i"),
    toFlag("single line", "s"),
    toFlag("extended", "e")],
  },
  mutations: {

  },
  actions: {

  },
});
